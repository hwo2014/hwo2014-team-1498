import json
import socket
import sys
import math
import itertools

def calc_velocity(pieces, last_piece_position, current_piece_position):
    """
    Args:
      pieces: data['race']['track']['pieces'] of gameInit message
      last_piece_position, current_piece_position:
        data[_]['carPositions']['piecePosition'] of carPosition message
    """
    if last_piece_position is None:
        return current_piece_position['inPieceDistance']
    last_piece_idx = last_piece_position['pieceIndex']
    current_piece_idx = current_piece_position['pieceIndex']
    if last_piece_idx == current_piece_idx:
        # In the same piece
        return current_piece_position['inPieceDistance'] - last_piece_position['inPieceDistance']
    else:
        # Between two pieces
        last_piece = pieces[last_piece_idx]
        if 'length' in last_piece:
            distance_in_last_piece = last_piece['length'] - last_piece_position['inPieceDistance']
        else:
            distance_in_last_piece = last_piece['lengths'][last_piece_position['lane']['endLaneIndex']] - last_piece_position['inPieceDistance']
        # If last piece has a switch, calculation here is not accurate.
        # distance_in_last_piece += 2.06 * (last_piece_position['lane']['endLaneIndex'] - last_piece_position['lane']['startLaneIndex'])
        # print 'distance_in_last_piece', distance_in_last_piece
        # print 'distance_in_current_piece', current_piece_position['inPieceDistance']
        return distance_in_last_piece + current_piece_position['inPieceDistance']

def get_next_lane_index(current_lane_idx, direction):
    if direction == NoobBot.LEFT:
        return current_lane_idx - 1
    elif direction == NoobBot.RIGHT:
        return current_lane_idx + 1
    else:
        return current_lane_idx

def is_straight(piece):
    return 'radius' not in piece

def find_first_piece_index_of_longest_straight_pieces(pieces):
    pieces_concat = pieces[:]
    if is_straight(pieces[-1]):
        for piece in pieces:
            if not is_straight(piece):
                break
            pieces_concat.append(piece)
    max_length = 0.0
    piece_idx_of_max_length = 0
    piece_idx = 0
    while True:
        if piece_idx == len(pieces_concat) - 1:
            print piece_idx_of_max_length, max_length
            return piece_idx_of_max_length
        while not is_straight(pieces_concat[piece_idx]):
            if piece_idx == len(pieces_concat) - 1:
                break
            piece_idx += 1
        length = 0.0
        tmp_piece_idx = piece_idx
        while is_straight(pieces_concat[tmp_piece_idx]):
            length += pieces_concat[tmp_piece_idx]['length']
            if tmp_piece_idx == len(pieces_concat) - 1:
                break
            tmp_piece_idx += 1
        if length > max_length:
            max_length = length
            piece_idx_of_max_length = piece_idx
        piece_idx = tmp_piece_idx
    assert False

def estimate_parameters(velocity_tick1, velocity_tick2, velocity_tick3):
    if velocity_tick1 == velocity_tick2: return None, None
    kmbuff=2*velocity_tick2-velocity_tick1-velocity_tick3
    if kmbuff==0: return None, None
    k_divide_m = kmbuff / (velocity_tick2 - velocity_tick1)
    engine_power = (velocity_tick2-velocity_tick1)/k_divide_m+velocity_tick1
    print "k/m :", k_divide_m, ", ENGINE POWER :", engine_power
    return k_divide_m, engine_power

def estimate_dynamic_friction(radius, velocity_beforeslip, first_slipangle):
    # degree_angular_velocity = math.degrees(velocity_beforeslip/radius)
    # velocity_threshold = math.radians(degree_angular_velocity - abs(first_slipangle)) * radius
    # dynamic_friction = velocity_threshold * velocity_threshold / radius
    fric_coef = 0.3
    velocity_threshold = velocity_beforeslip - fric_coef * math.sqrt(radius) * abs(first_slipangle)
    dynamic_friction = velocity_threshold * velocity_threshold / radius
    print "DYNAMIC FRICTION :", dynamic_friction,
    print ", SLIP SPEED ( r =", radius, ") :", velocity_threshold
    return dynamic_friction

class NoobBot(object):
    LEFT = 'Left'
    RIGHT = 'Right'
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        # Information of the course and status of cars.
        # Must be initialized for every race.
        self.pieces = None
        self.lanes = None
        self.position_history = {}
        self.velocity_history = {}
        self.optimal_switch_directions = {}
        self.available_turbo = None
        self.turbo_sent = False
        self.turbo_remaining_ticks = 0
        self.turbo_factor = 1
        self.being_crashed = False
        self.max_laps = None
        self.is_moved = False
        # Estimated parameters.
        # Must be initialized for every qualifying round.
        self.engine_power = None
        self.k_divide_m = None
        self.dynamic_friction = None
        self.can_obtain_data = False
        # self.d2angle_coef = 9.26
        # self.angle_coef = 0.0745
        # self.velocity_coef = 4.82
        self.d2angle_coef = 9.17
        self.angle_coef = 0.0804
        self.velocity_coef = 3.4
        self.angle_buffer = 0
        self.angle_uncertainty_coef = 1.0
        self.angle_prediction = {}
        self.crashes = {}

    def msg(self, msg_type, data, tick=None):
        print "msg:", msg_type, data, tick
        if tick:
            self.send(json.dumps({"msgType": msg_type, "data": data, 'gameTick': tick}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, track_name, car_count):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                     "trackName": track_name,
                                     "carCount": car_count})

    def throttle(self, throttle, tick=None):
        self.msg("throttle", throttle, tick)

    def switch(self, direction):
        if direction:
            print 'Switch to', direction
            self.msg("switchLane", direction)

    def turbo(self):
        print "Use turbo"
        self.msg("turbo", "Pow pow pow pow pow")
        self.turbo_sent = True

    def ping(self):
        self.msg("ping", {})

    def run(self, track_name=None, car_count=None):
        if track_name and car_count:
            self.join_race(track_name, car_count)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        # self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()
    
    def get_next_piece_index(self, current_piece_idx):
        if current_piece_idx == len(self.pieces) - 1:
            return 0
        else:
            return current_piece_idx + 1

    def get_prev_piece_index(self, current_piece_idx):
        if current_piece_idx == 0:
            return len(self.pieces) - 1
        else:
            return current_piece_idx - 1

    def get_current_piece_index(self):
        return self.position_history[self.name][-1]['piecePosition']['pieceIndex']

    def get_pasttick_piece_index(self, n):
        return self.position_history[self.name][-1 - n]['piecePosition']['pieceIndex']

    def get_current_lane_index(self):
        return self.position_history[self.name][-1]['piecePosition']['lane']['endLaneIndex']

    def calc_curve_min_radius(self, curve_piece_idx):
        return min([self.calc_curve_radius(curve_piece_idx, lane_idx) for lane_idx in xrange(0, len(self.lanes))])

    def calc_curve_radius(self, curve_piece_idx, lane_idx):
        curve_piece = self.pieces[curve_piece_idx]
        if curve_piece["angle"] > 0:
            # Curve right
            lane_radius = curve_piece["radius"] - self.lanes[lane_idx]["distanceFromCenter"]
        else:
            # Curve left
            lane_radius = curve_piece["radius"] + self.lanes[lane_idx]["distanceFromCenter"]
        return lane_radius

    def get_current_angle(self):
        return self.position_history[self.name][-1]['angle']

    def calc_slip_velocity(self, radius):
        return math.sqrt(self.dynamic_friction * radius)

    def detect_car_movement(self):
        """
        If car position of past1 tick is not 0,
        this function returns True
        """
        print self.velocity_history[self.name]
        if len(self.velocity_history[self.name]) == 1:
            return False
        elif self.velocity_history[self.name][-2] != 0:
            return True
        else:
            return False

    def predict_angle(self, current_angle, past1_angle, current_velocity, throttle, current_piece_idx, distance_in_piece):
        next_velocity = self.predict_velocity(current_velocity, throttle)
        if self.is_on_curve(current_piece_idx):
            # current_radius = self.calc_curve_radius(current_piece_idx)
            current_radius = self.calc_curve_min_radius(current_piece_idx)
            slip_velocity = self.calc_slip_velocity(current_radius)
            velocity_offset = self.velocity_coef * max(next_velocity - slip_velocity, 0)
            current_piece = self.pieces[current_piece_idx]
            if current_piece['angle'] < 0:
                velocity_offset *= -1
        else:
            velocity_offset = 0
        past1_dangle = current_angle - past1_angle
        past1_d2angle = (velocity_offset - past1_dangle - self.angle_coef * past1_angle) / self.d2angle_coef
        current_dangle = past1_dangle + past1_d2angle
        next_angle = current_angle + current_dangle
        # return next_angle, next_velocity
        # return sign(next_angle) * abs(next_angle) ** self.angle_uncertainty_coef, next_velocity
        def sign(x):
            if x >= 0:
                return 1
            else:
                return -1
        return sign(next_angle) * abs(next_angle) ** 1.002, next_velocity

    def predict_velocity(self, current_velocity, next_throttle):
        ep, k_m = self.engine_power, self.k_divide_m
        next_velocity = (1 - k_m) * current_velocity + ep * k_m * next_throttle
        return next_velocity

    def predict_velocity_n_ticks_after(self, n, current_velocity, throttle):
        ep, k_m = self.engine_power, self.k_divide_m
        alpha = ep * throttle
        velocity_n_ticks_after = (current_velocity - alpha) * math.pow(1 - k_m, n) + alpha
        return velocity_n_ticks_after

    def calc_ticks_to_reach_velocity(self, current_velocity, aimed_velocity, throttle):
        # If you want to calc minimum ticks, throttle must be set 1.0
        ep, k_m = self.engine_power, self.k_divide_m
        numerator = aimed_velocity - ep * throttle
        denominator = current_velocity - ep * throttle
        ticks = math.log(numerator/denominator, 1 - k_m)
        return math.floor(ticks)

    def calc_throttle_to_reach_velocity(self, current_velocity, aimed_velocity, ticks):
        ep, k_m = self.engine_power, self.k_divide_m
        attenuation = pow(1-k_m, ticks)
        numerator = aimed_velocity - current_velocity * attenuation
        denominator = ep * (1 - attenuation)
        throttle = numerator / denominator
        print ep, k_m, attenuation, throttle
        return throttle

    def calc_total_length_of_switch_plan(self, switch_plan, current_piece_idx, current_lane_idx):
        """
        Args:
          switch_plan: list that contains directions for switches
                       e.g. ['Right', None, None, ...]
        """
        next_piece_idx = self.get_next_piece_index(current_piece_idx)
        piece_idx = next_piece_idx
        total_length = 0.0
        switch_plan_to_pop = list(switch_plan)
        switch_plan_to_pop.reverse()
        while switch_plan_to_pop:
            piece = self.pieces[piece_idx]
            total_length += piece['lengths'][current_lane_idx]
            if piece_idx in self.switches:
                switch = switch_plan_to_pop.pop()
                if switch == NoobBot.LEFT:
                    if current_lane_idx == 0:
                        return sys.maxint
                    else:
                        current_lane_idx -= 1
                elif switch == NoobBot.RIGHT:
                    if current_lane_idx == len(self.lanes) - 1:
                        return sys.maxint
                    else:
                        current_lane_idx += 1
            piece_idx = self.get_next_piece_index(piece_idx)
        return total_length

    def get_next_switch_piece_index(self, current_piece_idx):
        larger = [piece_idx for piece_idx in self.switches if piece_idx > current_piece_idx]
        if larger:
            return larger[0]
        else:
            return self.switches[0]

    def get_next_curve_piece_index(self, current_piece_idx):
        piece_idx = self.get_next_piece_index(current_piece_idx)
        while is_straight(self.pieces[piece_idx]):
            piece_idx = self.get_next_piece_index(piece_idx)
        return piece_idx

    def get_piece_range(self, first_piece_idx, last_piece_idx):
        piece_range = []
        piece_idx = first_piece_idx
        while True:
            piece_range.append(piece_idx)
            if piece_idx != last_piece_idx:
                break
            piece_idx = self.get_next_piece_index(piece_idx)
        return piece_range

    def calc_distance_from_piece(self, car_position, piece_idx_to_start, lane_idx):
        total_distance = 0.0
        piece_idx = piece_idx_to_start
        while piece_idx != car_position['pieceIndex']:
            piece = self.pieces[piece_idx]
            total_distance += piece['lengths'][lane_idx]
            piece_idx = self.get_next_piece_index(piece_idx)
        total_distance += car_position['inPieceDistance']
        return total_distance

    def calc_distance_to_piece(self, car_position, dest_piece_idx, lane_idx):
        total_distance = self.pieces[car_position['pieceIndex']]['lengths'][lane_idx] - car_position['inPieceDistance']
        piece_idx = self.get_next_piece_index(car_position['pieceIndex'])
        while piece_idx != dest_piece_idx:
            piece = self.pieces[piece_idx]
            total_distance += piece['lengths'][lane_idx]
            piece_idx = self.get_next_piece_index(piece_idx)
        return total_distance

    def calc_piece_index_ahead(self, car_position, lane_idx, distance):
        current_piece_idx = car_position['pieceIndex']
        remaining_distance_in_current_piece = self.pieces[current_piece_idx]['lengths'][lane_idx] - car_position['inPieceDistance']
        if distance <= remaining_distance_in_current_piece:
            return current_piece_idx
        remaining_distance = distance - remaining_distance_in_current_piece
        piece_idx = current_piece_idx + 1
        while remaining_distance >= self.pieces[piece_idx]['lengths'][lane_idx]:
            remaining_distance -= self.pieces[piece_idx]['lengths'][lane_idx]
            piece_idx = self.get_next_piece_index(piece_idx)
        return piece_idx

    def is_lane_blocked(self, lane_idx, current_piece_idx):
        next_switch_piece_idx = self.get_next_switch_piece_index(current_piece_idx)
        next_next_switch_piece_idx = self.get_next_switch_piece_index(next_switch_piece_idx)
        relevant_range = self.get_piece_range(next_switch_piece_idx, next_next_switch_piece_idx)
        for name, position_history in self.position_history.iteritems():
            if name == self.name:
                continue
            opponent_position = position_history[-1]['piecePosition']
            opponent_piece_idx = opponent_position['pieceIndex']
            if opponent_piece_idx not in relevant_range:
                continue
            opponent_lane_idx = opponent_position['lane']['endLaneIndex']
            if opponent_lane_idx != lane_idx:
                continue
            # There is an opponent car in the same lane ahead of mine.
            # The lane is blocked if at least one of two below are true:
            #   1. The opponent car is very close.
            #   2. The opponent car is slower than mine and will be hit by mine
            #      before the next switching chance.
            my_position = self.position_history[self.name][-1]['piecePosition']
            distance_diff = self.calc_distance_from_piece(opponent_position, current_piece_idx, lane_idx) - self.calc_distance_from_piece(my_position, current_piece_idx, lane_idx)
            # Check case 1
            if distance_diff < self.cars[0]['dimensions']['length'] * 1.2:
                print "Lane is blocked by a very close car."
                return True
            # Check case 2
            my_velocity = self.velocity_history[self.name][-1]
            opponent_velocity = self.velocity_history[name][-1]
            if opponent_velocity < my_velocity:
                velocity_diff = my_velocity - opponent_velocity
                time_to_hit = distance_diff / velocity_diff
                travel_length_to_hit = time_to_hit * my_velocity
                piece_idx_to_hit = self.calc_piece_index_ahead(my_position, lane_idx, travel_length_to_hit)
                if piece_idx_to_hit in relevant_range:
                    print "Lane is blocked by a slower car."
                    return True
        return False

    def is_on_curve(self, current_piece_idx):
        current_piece = self.pieces[current_piece_idx]
        return current_piece.has_key("angle")
        
    def switch_lanes_if_necessary(self):
        if len(self.position_history[self.name]) == 1 or \
                self.position_history[self.name][-2]['piecePosition']['pieceIndex'] != self.position_history[self.name][-1]['piecePosition']['pieceIndex']:
            # Entering new lanes
            current_piece_idx = self.get_current_piece_index()
            print self.pieces[current_piece_idx]
            current_lane_idx = self.get_current_lane_index()
            if (current_piece_idx, current_lane_idx) in self.optimal_switch_directions:
                # Next piece has a switch
                for direction in self.optimal_switch_directions[(current_piece_idx, current_lane_idx)]:
                    current_lane_idx = self.get_current_lane_index()
                    next_optimal_lane_idx = get_next_lane_index(current_lane_idx, direction)
                    if not self.is_lane_blocked(next_optimal_lane_idx, current_piece_idx):
                        self.switch(direction)
                        return direction is not None
                # All lanes are blocked, so select the shortest one
                optimal_dir = self.optimal_switch_directions[(current_piece_idx, current_lane_idx)][0]
                self.switch(optimal_dir)
                return direction is not None
        return False

    def on_turbo_available(self, data):
        print 'turbo available:', data
        if not self.being_crashed:
            self.available_turbo = (data['turboDurationTicks'], data['turboFactor'])
            self.turbo_sent = False

    def on_turbo_start(self, data):
        print 'turbo start:', data
        if data['name'] == self.name:
            duration, factor = self.available_turbo
            self.available_turbo = None
            self.turbo_remaining_ticks = duration
            self.turbo_factor = factor
            self.turbo_sent = False

    def on_turbo_end(self, data):
        print 'turbo end:', data
        if data['name'] == self.name:
            self.turbo_remaining_ticks = 0
            self.turbo_factor = 1

    def get_current_lap(self):
        return self.position_history[self.name][-1]['piecePosition']['lap']

    def on_car_positions(self, data, tick):
        if self.turbo_remaining_ticks > 0:
            self.turbo_remaining_ticks -= 1
        # Record current position
        for car_position in data:
            name = car_position["id"]["name"]
            self.position_history[name].append(car_position)
        for name, position_history in self.position_history.iteritems():
            # Record current velocity
            if len(position_history) >= 2:
                # If last piece has a switch, accurate velocity cannot be obtained,
                # so approximates it from previous velocity
                last_piece_idx = position_history[-2]['piecePosition']['pieceIndex']
                if last_piece_idx != self.get_current_piece_index() and last_piece_idx in self.switches:
                    velocity = 2 * self.velocity_history[name][-1] - self.velocity_history[name][-2]
                else:
                    velocity = calc_velocity(self.pieces, position_history[-2]['piecePosition'], position_history[-1]['piecePosition'])
            else:
                velocity = calc_velocity(self.pieces, None, position_history[-1]['piecePosition'])
            self.velocity_history[name].append(velocity)
        print "t:", tick, "piece_idx:", self.get_current_piece_index(), "v:", self.velocity_history[self.name][-1], "angle:", self.position_history[self.name][-1]['angle']
        print 'piece:', self.pieces[self.get_current_piece_index()]
        if tick == None:
            self.throttle(1.0)
            return

        # Estimate parameters related to velocity
        if self.k_divide_m is None:
            if (not self.is_moved):
                self.is_moved = self.detect_car_movement()
            if self.is_moved and tick > 2:
                velocity_tick1 = self.velocity_history[self.name][-3]
                velocity_tick2 = self.velocity_history[self.name][-2]
                velocity_tick3 = self.velocity_history[self.name][-1]
                self.k_divide_m, self.engine_power = estimate_parameters(velocity_tick1, velocity_tick2, velocity_tick3)
            self.throttle(1.0, tick)
            print 'throttle=1.0 for estimating parameters related to velocity'
            return

        # Estimate parameters related to slip angle
        # You have to keep velocity enough slow to enter first curve without slip
        if self.dynamic_friction is None:
            past1_velocity = self.velocity_history[self.name][-2]
            cur_angle = self.position_history[self.name][-1]['angle']
            past1_angle = self.position_history[self.name][-2]['angle']
            current_piece_idx = self.get_current_piece_index()
            if past1_angle == 0 and cur_angle != 0:
                radius = self.calc_curve_radius(self.get_current_piece_index(), self.get_current_lane_index())
                self.dynamic_friction = estimate_dynamic_friction(radius, past1_velocity, cur_angle)
            # if is_straight(self.pieces[self.get_current_piece_index()]):
            #     print 'throttle=0.4 for estimating parameters related to slip angle'
            #     self.throttle(0.4, tick)
            # else:
            #     print 'throttle=1.0 for estimating parameters related to slip angle'
            #     self.throttle(1.0, tick)
            print 'throttle=1.0 for estimating parameters related to slip angle'
            self.throttle(1.0, tick)
            return
        
            
        # self.throttle(throttle, tick)
        # return

        # Collect data for estimate parameters related to slip angle
        # past2_angle = self.position_history[self.name][-3]['angle']
        # cur_idx = self.get_pasttick_piece_index(0)
        # past2_idx = self.get_pasttick_piece_index(2)
        # self.can_obtain_data = (past2_angle != 0) and self.is_on_curve(cur_idx) and (cur_idx == past2_idx) 
        # # #self.can_obtain_data = (past2_angle != 0) and (cur_idx == past2_idx) 

        # if self.can_obtain_data:
        #     past1_angle = self.position_history[self.name][-2]['angle']
        #     cur_angle = self.position_history[self.name][-1]['angle']
        #     radius = self.calc_curve_radius(self.get_current_piece_index())
        #     past2_velocity = self.velocity_history[self.name][-3]
        #     delta_theta1 = past1_angle - past2_angle
        #     delta2_theta = cur_angle - 2 * past1_angle + past2_angle
        #     write_string = str(self.k_divide_m) + ' ' + str(self.engine_power) + ' ' + str(self.dynamic_friction) + ' '
        #     write_string += str(radius) + ' ' + str(past2_velocity) + ' ' + \
        #     str(past2_angle) + ' ' + str(delta_theta1) + ' ' + str(delta2_theta)
        #     write_string += '\n'
        #     f.write(write_string)
        # return 

        # # Adjust dynamic friction
        # if tick in self.angle_prediction:
        #     predicted_angle = self.angle_prediction[tick]
        #     actual_angle = self.position_history[self.name][-1]['angle']
        #     if abs(predicted_angle) > abs(actual_angle):
        #         self.dynamic_friction += 0.001
        #         # self.dynamic_friction -= 0.01
        #     if abs(predicted_angle) < abs(actual_angle):
        #         self.dynamic_friction -= 0.001
        #         # self.dynamic_friction += 0.01
        # print "dynamic friction:", self.dynamic_friction

        if self.being_crashed:
            self.ping()
            return

        # Decide whether to switch or not
        if self.switch_lanes_if_necessary():
            # Only one message can be accepted for one tick, so return
            return
        elif self.available_turbo and not self.turbo_sent:
            if self.get_current_piece_index() == self.first_piece_idx_of_longest_straight_pieces:
                # Use turbo in the longest straight pieces
                self.turbo()
                return
            elif self.get_current_piece_index() > self.first_piece_idx_of_longest_straight_pieces and self.get_current_lap() == self.max_laps - 1 and is_straight(self.pieces[self.get_current_piece_index()]):
                # Now it's the final lap and the logest straight pieces are gone,
                # so use turbo at any straight piece.
                self.turbo()
                return
        throttle = self.calc_throttle_to_avoid_crash(1000)
        print 'calc throttle:', throttle

        # Demo usage of predict_angle
        current_angle = self.position_history[self.name][-1]['angle']
        past1_angle = self.position_history[self.name][-2]['angle']
        current_velocity = self.velocity_history[self.name][-1]
        current_piece_idx = self.get_current_piece_index()
        in_piece_distance = self.position_history[self.name][-1]['piecePosition']['inPieceDistance']
        # print in_piece_distance
        predicted_angle, predicted_velocity = self.predict_angle(current_angle, past1_angle, current_velocity, throttle, current_piece_idx, in_piece_distance)
        print "Predicted (angle, velocity) is", predicted_angle, predicted_velocity
        self.angle_prediction[tick + 1] = predicted_angle

        self.throttle(throttle, tick)

    def calc_throttle_to_avoid_crash(self, distance_to_predict):
        """
        Calc a throttle value that will not cause crash in traveling
        a certain distance from the current position.
        """
        current_piece_idx = self.get_current_piece_index()
        car_position = self.position_history[self.name][-1]['piecePosition']
        lane_idx = car_position['lane']['endLaneIndex']
        next_curve_piece_idx = self.get_next_curve_piece_index(current_piece_idx)
        # print 'next_curve_piece_idx:', next_curve_piece_idx
        if self.get_current_lap() == self.max_laps - 1 and current_piece_idx > next_curve_piece_idx:
            # There is a goal before the next curve, so just accelerate
            return 1.0
        # distance_to_curve = self.calc_distance_to_piece(car_position, next_curve_piece_idx, lane_idx)
        current_velocity = self.velocity_history[self.name][-1]
        current_angle = self.position_history[self.name][-1]['angle']
        past_angle = self.position_history[self.name][-2]['angle']
        current_distance_in_piece = self.position_history[self.name][-1]['piecePosition']['inPieceDistance']
        throttle = 1.0
        throttle_step = 0.1
        # angle_threshold = 59 - self.angle_buffer
        angle_threshold = 59
        crash_penalty_factor = 10
        while throttle > throttle_step:
            # Check if outputting throttle=throttle now and throttle=0 later
            # will cause crash in the future.
            tmp_angle, tmp_v = self.predict_angle(current_angle, past_angle, current_velocity, throttle * self.turbo_factor, current_piece_idx, current_distance_in_piece)
            # tmp_angle *= self.angle_uncertainty_coef
            print "predicted for throttle=", throttle, ":", tmp_angle, tmp_v
            if abs(tmp_angle) > max(angle_threshold - self.crashes.get(current_piece_idx, 0) * crash_penalty_factor, 1):
                # This throttle will cause immediate crash
                throttle -= throttle_step
                continue
            tmp_distance = tmp_v
            tmp_past_angle = current_angle
            ticks = 0
            tmp_piece_idx, tmp_distance_in_piece = self.get_next_position(lane_idx, current_piece_idx, current_distance_in_piece, tmp_v)
            # Continue outputting throttle=0
            while True:
                (tmp_angle, tmp_v), tmp_past_angle = self.predict_angle(tmp_angle, tmp_past_angle, tmp_v, 0.0, tmp_piece_idx, tmp_distance_in_piece), tmp_angle

                if ticks < 5:
                    print "  predicted:", tmp_angle, tmp_v, tmp_past_angle, tmp_piece_idx
                tmp_distance += tmp_v
                ticks += 1
                if ticks > 10 and tmp_v < 0.1:
                    # Almost stopped. This throttle is safe.
                    print "almost stopped"
                    return throttle
                elif abs(tmp_angle) > max(angle_threshold - self.crashes.get(tmp_piece_idx, 0) * crash_penalty_factor, 1):
                    print "crashed", tmp_angle, tmp_v, tmp_past_angle, tmp_piece_idx
                    # Crashed. Try smaller throttle.
                    throttle -= throttle_step
                    break
                elif tmp_distance > distance_to_predict:
                    print "reached destination"
                    # Reached destination without crash. This throttle is safe.
                    return throttle
                tmp_piece_idx, tmp_distance_in_piece = self.get_next_position(lane_idx, tmp_piece_idx, tmp_distance_in_piece, tmp_v)
        return 0.0

    def get_next_position(self, lane_idx, current_piece_idx, distance_in_piece, next_velocity):
        """
        Returns: (next_piece_idx, distance_in_next_piece)
        """
        # current_piece_length = self.pieces[current_piece_idx]['lengths']
        current_piece_length = self.pieces[current_piece_idx]['lengths'][lane_idx]
        if distance_in_piece + next_velocity > current_piece_length:
            # Move to the next piece
            return (self.get_next_piece_index(current_piece_idx), distance_in_piece + next_velocity - current_piece_length)
        else:
            # Stay in the same piece
            return (current_piece_idx, distance_in_piece + next_velocity)

    # def calc_throttle_to_avoid_slip_at_current_curve(self):
    #     additional_slip_velocity = 0.3
    #     current_piece_idx = self.get_current_piece_index()
    #     slip_velocity = self.calc_slip_velocity(self.calc_curve_radius(current_piece_idx)) + additional_slip_velocity
    #     print 'slip_velocity:', slip_velocity
    #     throttle = 1.0
    #     while self.predict_velocity(self.velocity_history[self.name][-1], throttle * self.turbo_factor) > slip_velocity:
    #         throttle -= 0.1
    #         if throttle < 0.0:
    #             return 0.0
    #     print 'predict_velocity for', throttle, ':', self.predict_velocity(self.velocity_history[self.name][-1], throttle * self.turbo_factor)
    #     return throttle

    # def calc_throttle_to_avoid_slip(self):
    #     current_piece_idx = self.get_current_piece_index()
    #     current_piece = self.pieces[current_piece_idx]
    #     throttles = []
    #     # In first half of curve, keep velocity below slip velocity
    #     # if not is_straight(current_piece) and float(car_position['inPieceDistance']) / current_piece['lengths'][lane_idx] < 0.5:
    #     if not is_straight(current_piece):
    #         throttles.append(self.calc_throttle_to_avoid_slip_at_current_curve())
    #     throttles.append(self.calc_throttle_to_avoid_slip_at_next_curve())
    #     return min(throttles)

    def on_crash(self, data):
        print("Someone crashed")
        if data['name'] == self.name:
            self.being_crashed = True
        if self.are_parameters_estimated():
            self.crashes[self.get_current_piece_index()] = self.crashes.get(self.get_current_piece_index(), 0) + 1
            self.angle_uncertainty_coef += 0.002
            self.angle_buffer += 2
            print "angle buffer incremented to", self.angle_buffer
            print "angle uncertainty coef incremented to", self.angle_uncertainty_coef
        # self.ping()
        
    def on_spawn(self, data):
        print("Someone spawned")
        if data['name'] == self.name:
            self.being_crashed = False
        # self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        # Initialize parameters
        self.pieces = None
        self.lanes = None
        self.position_history = {}
        self.velocity_history = {}
        self.optimal_switch_directions = {}
        self.available_turbo = None
        self.turbo_sent = False
        self.turbo_remaining_ticks = 0
        self.turbo_factor = 1
        self.being_crashed = False
        self.max_laps = None
        self.is_moved = False
        self.angle_prediction = {}

        print 'gameInit:', data
        self.pieces = data['race']['track']['pieces']
        self.lanes = data['race']['track']['lanes']
        self.cars = data['race']['cars']
        self.switches = []

        # Save indices of pieces that have switches
        for i, piece in enumerate(self.pieces):
            if 'switch' in piece and piece['switch']:
                self.switches.append(i)

        # Calc lengths of lanes in curves
        self.init_lengths_of_lanes()
        print self.pieces

        # Init velocity history
        for car in self.cars:
            name = car['id']['name']
            self.position_history[name] = []
            self.velocity_history[name] = []

        # Calc optimal switch directions in advance
        self.init_optimal_switch_directions()

        self.first_piece_idx_of_longest_straight_pieces = find_first_piece_index_of_longest_straight_pieces(self.pieces)
        if 'laps' in data['race']['raceSession']:
            # Actual race.
            self.max_laps = data['race']['raceSession']['laps']
        else:
            # In qualifying session, 'laps' property is not available.
            self.max_laps = sys.maxint
            # Estimated parameters must be initialized
            self.dynamic_friction = None
            self.k_divide_m = None
            self.engine_power = None
            self.angle_buffer = 0
            self.angle_uncertainty_coef = 1
            self.crashes = {}

    def init_lengths_of_lanes(self):
        """
        Add 'lengths' property for each piece.
        """
        for piece in self.pieces:
            if "length" not in piece:
                piece["lengths"] = []
                for lane in self.lanes:
                    if piece["angle"] > 0:
                        # Curve right
                        lane_radius = piece["radius"] - lane["distanceFromCenter"]
                    else:
                        # Curve left
                        lane_radius = piece["radius"] + lane["distanceFromCenter"]
                    angle_rad = abs(piece["angle"] / 180 * math.pi)
                    piece["lengths"].append(lane_radius * angle_rad)
            else:
                piece['lengths'] = [piece['length']] * len(self.lanes)

    def init_optimal_switch_directions(self):
        """
        Calc optimal switch direction for each switch and lane in advance
        """
        print "start to calc optimal swith directions..."
        max_switch_count_to_predict = 3
        for switch_piece_idx in self.switches:
            piece_idx = self.get_prev_piece_index(switch_piece_idx)
            for lane_idx in xrange(0, len(self.lanes)):
                switch_plans = [switch_plan for switch_plan in itertools.product([None, NoobBot.LEFT, NoobBot.RIGHT], repeat=min([max_switch_count_to_predict, len(self.switches)]))]
                switch_plans_with_lengths = sorted([(switch_plan, self.calc_total_length_of_switch_plan(switch_plan, piece_idx, lane_idx)) for switch_plan in switch_plans], key=lambda x: x[1])
                first_directions = [switch_plan_with_length[0][0] for switch_plan_with_length in switch_plans_with_lengths if switch_plan_with_length[1] != sys.maxint]
                uniq_first_directions = sorted(set(first_directions), key=first_directions.index)
                self.optimal_switch_directions[(piece_idx, lane_idx)] = uniq_first_directions
        print 'optimal switch directions:', self.optimal_switch_directions

    def are_parameters_estimated(self):
        return self.dynamic_friction and self.k_divide_m and self.engine_power

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type == 'carPositions':
                tick = None
                if 'gameTick' in msg:
                    tick = msg['gameTick']
                msg_map[msg_type](data, tick)
            elif msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5 and len(sys.argv) != 7:
        print("Usage: ./run host port botname botkey [track_name car_count]")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        if len(sys.argv) == 7:
            track_name, car_count = sys.argv[5:7]
            bot.run(track_name, car_count)
        else:
            bot.run()
